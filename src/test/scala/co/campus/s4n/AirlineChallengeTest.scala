package co.campus.s4n

import co.campus.s4n.airlines_challenge.{Airline, AirlineDelay, Airport}
import org.apache.spark.sql.DataFrame
import org.scalatest.FunSpec

class AirlineChallengeTest extends FunSpec with SparkSessionTestWrapper {

  import spark.implicits._
  it ("counts the number of flights by airline") {
    val inputSeq = Seq(AirlineDelay("test", "test", "Bogotá", "L/A", None, None),
      AirlineDelay("test", "UA", "Bogotá", "L/A", None, Some("50.5")),
      AirlineDelay("test", "AA", "Bogotá", "L/A", None, Some("1.5")),
      AirlineDelay("test", "AA", "Bogotá", "L/A", None, Some("1.5")),
      AirlineDelay("test", "AA", "Bogotá", "L/A", None, Some("1.5")))

    val airlinesInput = Seq(
      Airline("UA", "United Air Lines Inc"),
      Airline("AA", "American Airlines Inc")
    )
    val df = inputSeq.toDF()
    val df1 = airlinesInput.toDF()
    val airlineDelay = df.as[AirlineDelay]
    val airlines = df1.as[Airline]
    val airlineStats = airlines_challenge.delayedAirlines(airlineDelay, None, airlines)
    airlineStats.foreach(println)
    assert(airlineStats.length.equals(2))
    assert(airlineStats.map(_.largeDelayFlights).head == 1)
    assert(airlineStats.map(_.totalFlights).sum == 4)
    assert(airlineStats.map(_.largeDelayFlights).sum == 1)

  }

  it ("counts the number of flights by airline for the 2009 year") {
    val inputSeq = Seq(AirlineDelay("test", "test", "Bogotá", "L/A", None, None),
      AirlineDelay("2009-01-01", "UA", "Bogotá", "L/A", None, Some("50.5")),
      AirlineDelay("2008-01-01", "AA", "Bogotá", "L/A", None, Some("1.5")),
      AirlineDelay("2009-01-01", "AA", "Bogotá", "L/A", None, Some("1.5")),
      AirlineDelay("2008-01-01", "AA", "Bogotá", "L/A", None, Some("1.5")))

    val airlinesInput = Seq(
      Airline("UA", "United Air Lines Inc"),
      Airline("AA", "American Airlines Inc")
    )
    val df = inputSeq.toDF()
    val df1 = airlinesInput.toDF()
    val airlineDelay = df.as[AirlineDelay]
    val airlines = df1.as[Airline]
    val airlineStats = airlines_challenge.delayedAirlines(airlineDelay, Some("2009"), airlines)
    airlineStats.foreach(println)
    assert(airlineStats.length.equals(2))
    assert(airlineStats.map(_.largeDelayFlights).head == 1)
    assert(airlineStats.map(_.totalFlights).sum == 2)

  }

  it ("find destinations for a given origin") {
    val inputSeq = Seq(("DCA", "EWR", "1058.0"),
      ("EWR", "IAD", "1509.0"),
      ("EWR", "DCA", "1509.0"),
      ("EWR", "DCA", "1710.0")
      )
//    val AirlineSchema = List(StructField("ORIGIN", StringType, false), StructField("DEST", StringType, false), StructField("DEP_TIME", StringType, false))

    val airportsInput = Seq(
      Airport("EWR", "Newark Liberty International Airport"),
      Airport("DCA", "Ronald Reagan Washington National Airport")
    )

    val destinationsDf = inputSeq.toDF("ORIGIN", "DEST", "DEP_TIME")
    destinationsDf.show(false)
    val df1 = airportsInput.toDF()

    val flightStats = airlines_challenge.destinations(destinationsDf, "EWR", df1.as[Airport])
    flightStats.foreach(println)

  }

  it ("find flights with more cancellations an its reasons") {
    val inputSeq = Seq((98, "DCA", "EWR", 0, "A"),
      (2336, "EWR", "IAD", 1, "B"),
      (840, "EWR", "DCA", 0, "C"),
      (612, "EWR", "DCA", 0, "D"),
      (612, "EWR", "DCA", 1, "B"),
      (612, "EWR", "DCA", 1, "D"),
      (806, "EWR", "DCA", 1, "B")
    )
    //    val AirlineSchema = List(StructField("ORIGIN", StringType, false), StructField("DEST", StringType, false), StructField("DEP_TIME", StringType, false))

    val airportsInput = Seq(
      Airport("EWR", "Newark Liberty International Airport"),
      Airport("DCA", "Ronald Reagan Washington National Airport")
    )
    val destinationsDf: DataFrame = inputSeq.toDF("OP_CARRIER_FL_NUM", "ORIGIN", "DEST", "CANCELLED", "CANCELLATION_CODE")
    destinationsDf.show(false)
    val df1 = airportsInput.toDF()

    val flightStats = airlines_challenge.flightInfo(destinationsDf)
    flightStats.foreach(println)

  }

  it ("find day with more cancellations") {
    val inputSeq = Seq((98, "DCA", "EWR", 0, "A", 1206, "2009-01-01"),
      (2336, "EWR", "IAD", 1, "B", 1206, "2010-01-01"),
      (840, "EWR", "DCA", 0, "C", 1624, "2009-02-01"),
      (612, "EWR", "DCA", 0, "D", 2123, "2009-05-12"),
      (612, "EWR", "DCA", 1, "B", 1206, "2015-01-08"),
      (612, "EWR", "DCA", 1, "D", 1941, "2019-03-09"),
        (806, "EWR", "DCA", 1, "B", 820, "2009-01-01")
    )
    //    val AirlineSchema = List(StructField("ORIGIN", StringType, false), StructField("DEST", StringType, false), StructField("DEP_TIME", StringType, false))

    val airportsInput = Seq(
      Airport("EWR", "Newark Liberty International Airport"),
      Airport("DCA", "Ronald Reagan Washington National Airport")
    )
    val destinationsDf = inputSeq.toDF("OP_CARRIER_FL_NUM", "ORIGIN", "DEST", "CANCELLED", "CANCELLATION_CODE", "ARR_DELAY", "FL_DATE")
    destinationsDf.show(false)
    val df1 = airportsInput.toDF()

    val daysWithDelays = airlines_challenge.daysWithDelays(destinationsDf)
    daysWithDelays.foreach(println)

  }

  it("doesn't matter") {
    val simpleData = Seq(("James","Sales","NY",90000,34,10000),
      ("Michael","Sales","NY",86000,56,20000),
      ("Robert","Sales","CA",81000,30,23000),
      ("Maria","Finance","CA",90000,24,23000),
      ("Raman","Finance","CA",99000,40,24000),
      ("Scott","Finance","NY",83000,36,19000),
      ("Jen","Finance","NY",79000,53,15000),
      ("Jeff","Marketing","CA",80000,25,18000),
      ("Kumar","Marketing","NY",91000,50,21000)
    )
    val df = simpleData.toDF("employee_name","department","state", "salary", "age","bonus")
    df.show()
    df.groupBy("department").sum("salary").show(false)
  }

}
