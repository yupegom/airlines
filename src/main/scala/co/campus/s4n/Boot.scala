package co.campus.s4n

import co.campus.s4n.airlines_challenge.{Airline, AirlineDelay, Airport}

object Boot extends App with SparkSessionWrapper {

  import spark.implicits._

  def load(path: String) =
    spark.read
      .format("csv")
      .option("sep", ",")
      .option("inferSchema", "true")
      .option("header", "true")
      .load(path)
  val airlineDelays = load("src/main/resources/delays/*.csv")
  val airlines = load("src/main/resources/airlines.csv")
  val airports = load("src/main/resources/airports.csv")
  // 1
  val airlineStats = airlines_challenge.delayedAirlines(airlineDelays.as[AirlineDelay], None, airlines.as[Airline])
  airlineStats.toStream.take(10).foreach( result => println(s"Result $result"))

  // 2
  val airlineDestinations = airlines_challenge.destinations(airlineDelays, "", airports.as[Airport])
  airlineDestinations.toStream.take(10).foreach( result => println(s"Result $result"))

  // 3
  val canceledFlights = airlines_challenge.flightInfo(airlineDelays)
  airlineDestinations.toStream.take(10).foreach( result => println(s"Result $result"))

  // 4
  val daysAndDelays = airlines_challenge.daysWithDelays(airlineDelays)
  daysAndDelays.toStream.take(5).foreach( result => println(s"Result $result"))
}
