package co.campus.s4n

import java.text.SimpleDateFormat

import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.collect_list
import org.apache.spark.sql.{DataFrame, Dataset}

import scala.collection.mutable

object airlines_challenge extends SparkSessionWrapper {

  import spark.implicits._

  //1. ¿Cuáles son las aerolíneas más cumplidas y las menos cumplidas de un año en especifico?
  //La respuesta debe incluir el nombre completo de la aerolínea, si no se envia el año debe calcular con
  //toda la información disponible.

  case class AirlineDelay(FL_DATE: String,
                          OP_CARRIER: String,
                          ORIGIN: String,
                          DEST: String,
                          DEP_DELAY: Option[String],
                          ARR_DELAY: Option[String])

  case class AirlineStats(name: String,
                          totalFlights: Long,
                          largeDelayFlights: Long,
                          smallDelayFlights: Long,
                          onTimeFlights: Long)

  case class Airline(IATA_CODE: String, AIRLINE: String)

  case class Airport(IATA_CODE: String, AIRPORT: String)
  /**
   * Un vuelo se clasifica de la siguiente manera:
   * ARR_DELAY < 5 min --- On time
   * 5 > ARR_DELAY < 45min -- small Delay
   * ARR_DELAY > 45min large delay
   *
   * Calcule por cada aerolinea el número total de vuelos durante el año (en caso de no recibir el parametro de todos los años)
   * y el número de ontime flights, smallDelay flighst y largeDelay flights
   *
   * Orderne el resultado por largeDelayFlights, smallDelayFlightsy, ontimeFlights
   *
   * @param ds
   * @param mayBeYear
   */
  def delayedAirlines(ds: Dataset[AirlineDelay], mayBeYear: Option[String], airlines: Dataset[Airline]): Seq[AirlineStats] = {
    ds.filter(airlineDelay => mayBeYear.fold(true)(year => airlineDelay.FL_DATE.startsWith(year)))
      .groupBy("OP_CARRIER")
      .agg(collect_list("ARR_DELAY")
      .as("delays"))
      .as[Delay]
      .map(delay => calculateDelay(delay.OP_CARRIER, delay.delays.map(_.toDouble)))
      .sort('largeDelayFlights.desc, 'smallDelayFlights.desc, 'onTimeFlights.desc)
      .join(airlines, 'carrier === 'IATA_CODE, "inner")
      .withColumnRenamed("AIRLINE", "name")
      .as[AirlineStats]
      .collect()
      .toSeq
  }

  case class Delay(OP_CARRIER: String, delays: List[String])

  case class Delays(carrier: String,
                    totalFlights: Long,
                    largeDelayFlights: Long,
                    smallDelayFlights: Long,
                    onTimeFlights: Long)

  def calculateDelay(name: String, arriveDelays: List[Double]): Delays = {
    arriveDelays.foldLeft(Delays(name, 0, 0, 0, 0)) { (acc, delay) =>
      delay match {
        case d if d <= 5 => acc.copy(onTimeFlights = acc.onTimeFlights+1, totalFlights = acc.totalFlights+1)
        case d if d > 5 && d <= 45 => acc.copy(smallDelayFlights = acc.smallDelayFlights+1, totalFlights =  acc.totalFlights +1)
        case _ => acc.copy(largeDelayFlights = acc.largeDelayFlights+1, totalFlights = acc.totalFlights+1)
      }
    }
  }

  // 2. Dado un origen por ejemplo DCA (Washington), ¿Cuáles son destinos y cuantos vuelos presentan durante la mañana, tarde y noche?
  case class FlightsStats(destination: String, morningFlights: Long, afternoonFlights: Long, nightFlights: Long)
  case class DepartureTimes(DEST: String, departureTimes: List[String])
  case class AirlineDestination(origin: String, destination: String)

  /**
   * Encuentre los destinos a partir de un origen, y de acuerdo a DEP_TIME clasifique el vuelo de la siguiente manera:
   * 00:00 y 8:00 - Morning
   * 8:01 y 16:00 - Afternoon
   * 16:01 y 23:59 - Night
   * @param ds
   * @param origin
   * @return
   */
  def destinations(ds: DataFrame, origin: String, airlines: Dataset[Airport]): Seq[FlightsStats] = {
      ds.filter('ORIGIN === origin)
      .groupBy("DEST")
      .agg(collect_list("DEP_TIME").as("departureTimes"))
      .as[DepartureTimes]
      .map(depTimes => classifyFlight(depTimes.DEST, depTimes.departureTimes.map(_.toDouble)))
      .join(airlines, 'destination === 'IATA_CODE, "inner")
      .select("AIRPORT", "morningFlights", "afternoonFlights", "nightFlights")
      .withColumnRenamed("AIRPORT", "destination")
      .as[FlightsStats]
      .collect()
      .toSeq
    }

  def classifyFlight(name: String, depTimes: List[Double]): FlightsStats = {
    depTimes.foldLeft(FlightsStats(name, 0, 0, 0)) { (acc, depTime) =>
      depTime match {
        case d if d >= 0 && d <= 800 => acc.copy(morningFlights = acc.morningFlights+1)
        case d if d > 800 && d <= 1600 => acc.copy(afternoonFlights = acc.afternoonFlights+1)
        case d if d > 1600 && d <= 2359 => acc.copy(nightFlights = acc.nightFlights+1)
        case _ => throw new RuntimeException("This should not happen")
      }
    }
  }

  //3. Encuentre ¿Cuáles son los números de vuelo (top 20)  que han tenido más cancelaciones y sus causas?

  case class CancelledFlight(number: Int, origin: String, destination: String, cancelled: Long, causes: List[(String,Int)])
  import org.apache.spark.sql.functions.sum
  import org.apache.spark.sql.functions.udf
  /**
   * Encuentre los vuelos más cancelados y cual es la causa mas frecuente
   * Un vuelo es cancelado si CANCELLED = 1
   * CANCELLATION_CODE A - Airline/Carrier; B - Weather; C - National Air System; D - Security
   *
   * @param ds
   */
  def flightInfo(ds: DataFrame): Seq[CancelledFlight] = {
    ds.filter('CANCELLED === 1)
      .groupBy("OP_CARRIER_FL_NUM", "ORIGIN", "DEST")
      .agg(sum("CANCELLED").as("cancelled"), collect_list("CANCELLATION_CODE").as("cancellations"))
      .withColumn("causes", udfClassifyCancellation($"cancellations"))
      .select('OP_CARRIER_FL_NUM.as("number"), 'ORIGIN.as("origin"), 'DEST.as("destination"), 'cancelled, 'causes)
      .orderBy('cancelled.desc)
      .as[CancelledFlight]
      .take(20)
      .toSeq

  }

  def udfClassifyCancellation:UserDefinedFunction = udf((reasons: mutable.WrappedArray[String]) => classifyCancellationReason(reasons))

  def classifyCancellationReason(reasons: mutable.WrappedArray[String]): List[(String, Int)] = {
    val translatedReasons: mutable.WrappedArray[String] = reasons.map {
      case "A" => "Airline/Carrier"
      case "B" => "Weather"
      case "C" => "National Air System"
      case "D" => "Security"
      case _ => ""
    }
      translatedReasons.groupBy(identity).map(x => (x._1, x._2.length)).toList
  }

  //4. ¿Que dias se presentan más retrasos históricamente?
  /**
   * Encuentre que dia de la semana se presentan más demoras,
   * sólo tenga en cuenta los vuelos donde ARR_DELAY > 45min
   *
   * @param ds
   * @return Una lista con tuplas de la forma (DayOfTheWeek, NumberOfDelays) i.e.("Monday",12356)
   */
  def daysWithDelays(ds:DataFrame) : List[(String, Long)] = {
    ds.filter('ARR_DELAY > 45)
        .withColumn("day", getDayOfWeek('FL_DATE))
      .groupBy("day")
      .count()
      .orderBy('count.desc)
      .as[(String, Long)]
      .collect()
      .toList

  }

  val getDayOfWeek:UserDefinedFunction =  udf((date: String) => calculteDayOfWeek(date))

  def calculteDayOfWeek(dateString: String): String = {
    val date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString)
    new SimpleDateFormat("EE").format(date)
  }


}

